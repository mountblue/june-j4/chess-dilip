var mainChessBoard = [];
var movementArray = [];
var movementCount = 0;
colArr = ["a", "b", "c", "d", "e", "f", "g", "h"];
var blackKingInCheck = false;
var whiteKingInCheck = false;
if (!window.localStorage.chessDBKey) {
    let newBoard = new ChessBoard();
    newBoard.createBoard();
    mainChessBoard = JSON.parse(localStorage.getItem("chessDBKey"));
} else {
    mainChessBoard = JSON.parse(localStorage.getItem("chessDBKey"));
}

function resetChess() {
    let newBoard = new ChessBoard();
    newBoard.createBoard();
    mainChessBoard = JSON.parse(localStorage.getItem("chessDBKey"));
    movementCount = 0;
    refreshBoard();
}

function refreshBoard() {
    mainChessBoard = JSON.parse(localStorage.getItem("chessDBKey"));
    document.getElementById('enamy-target-div').style.display = "none";
    document.getElementById('chessboard').innerHTML = "";
    for (let i = 0, row = 8; i < mainChessBoard.length; i++, row--) {
        movementArray[i] = [];
        for (let j = 0; j < mainChessBoard.length; j++) {
            var innerBox = document.createElement('div');
            innerBox.id = colArr[j] + row;
            movementArray[i][j] = colArr[j] + row;
            document.getElementById('chessboard').appendChild(innerBox);
            let chessPieceImage = mainChessBoard[i][j];
            if (chessPieceImage != -1) {
                var image = document.createElement("img");
                image.id = chessPieceImage;
                image.className = "chessPieces";
                image.setAttribute('src', "images/" + chessPieceImage + ".svg");
                image.setAttribute('onclick', "checkMovement(this)");
                image.setAttribute('ondragstart', 'dragStart(event)');
                if ((movementCount) % 2 == 1) {
                    if (chessPieceImage.includes("black")) {
                        image.setAttribute('draggable', "true");
                        document.getElementById('turn-name').innerHTML = "Black Turn";
                    } else {
                        image.setAttribute('draggable', "false");
                    }
                } else {
                    if (chessPieceImage.includes("white")) {
                        image.setAttribute('draggable', "true");
                        document.getElementById('turn-name').innerHTML = "White Turn";

                    } else {
                        image.setAttribute('draggable', "false");
                    }
                }
                innerBox.appendChild(image);
            }
            if ((i + j) % 2 == 0) {
                innerBox.className = 'black-block';
            } else {
                innerBox.className = 'white-block';
            }
        }
    }
}
refreshBoard();

function dragOver(event) {
    event.preventDefault();
}

function dragStart(event) {
    dragStartId = event.target.parentNode.id;
}

function drop(event) {
    event.preventDefault();
    var drop_index_i = 0;
    var drop_index_j = 0;
    var drag_index_i = 0;
    var drag_index_j = 0;
    var drag_stop_id;
    if (event.target.nodeName == "DIV") {
        drag_stop_id = event.target.id;
    } else {
        drag_stop_id = event.target.parentNode.id;
    }
    for (i = 0; i < movementArray.length; i++) {
        for (j = 0; j < movementArray.length; j++) {
            if (movementArray[i][j] == drag_stop_id) {
                drop_index_i = i;
                drop_index_j = j;
            }
            if (movementArray[i][j] == dragStartId) {
                drag_index_i = i;
                drag_index_j = j;
            }
        }
    }
    checkKingSafeOrNot();
    if (whiteKingInCheck || blackKingInCheck) {
        movementCount++;
        mainChessBoard[drop_index_i][drop_index_j] = mainChessBoard[drag_index_i][drag_index_j];
        mainChessBoard[drag_index_i][drag_index_j] = "-1";
        checkKingSafeOrNot();
        if (whiteKingInCheck || blackKingInCheck) {
            console.log("Save You King First.......");
            mainChessBoard = JSON.parse(localStorage.getItem("chessDBKey"));
            movementCount--;
        }
    } else {
        movementCount++;
        mainChessBoard[drop_index_i][drop_index_j] = mainChessBoard[drag_index_i][drag_index_j];
        mainChessBoard[drag_index_i][drag_index_j] = "-1";
        let pieceName = mainChessBoard[drop_index_i][drop_index_j];
        checkKingSafeOrNot();
        if ((pieceName.includes("black") && blackKingInCheck) || (pieceName.includes("white") && whiteKingInCheck)) {
            console.log("Don't move, your king will be in check");
            mainChessBoard = JSON.parse(localStorage.getItem("chessDBKey"));
            movementCount--;
        }
    }
    chessBoardDB.setItem("chessDBKey", JSON.stringify(mainChessBoard));
    refreshBoard();
    checkKingSafeOrNot();
}

function checkMovement(onePiece) {
    refreshBoard();
    document.getElementById('enemy-count').innerHTML = 0;
    var pieceId = onePiece.id;
    var pieceBoxId = onePiece.parentNode.id;
    if (document.getElementById(onePiece.id).draggable) {
        var movesArr = findMovement(pieceId, pieceBoxId);
        makeDraggableAndDropable(movesArr);
    }
}

function makeDraggableAndDropable(movesArr) {
    if (movesArr.length > 0) {
        let enemyTarget = 0;
        for (i = 0; i < movesArr.length; i++) {
            let dropableDivId = movementArray[movesArr[i][0]][movesArr[i][1]];
            document.getElementById(dropableDivId).setAttribute('ondragover', "dragOver(event)");
            document.getElementById(dropableDivId).setAttribute('ondrop', 'drop(event)');
            if (document.getElementById(dropableDivId).hasChildNodes()) {
                enemyTarget++;
                document.getElementById(dropableDivId).style.backgroundColor = "#F53C3C";
            } else {
                document.getElementById(dropableDivId).style.backgroundColor = "#BFFF95";
            }
            if (enemyTarget > 0) {
                document.getElementById('enamy-target-div').style.display = "block";
                document.getElementById('enemy-count').innerHTML = enemyTarget;
            }
        }
    }
}

checkKingSafeOrNot();

function checkKingSafeOrNot() {
    var blackArr = ["black-rook1", "black-knight1", "black-bishop1", "black-queen", "black-pawn1"];
    var whiteArr = ["white-rook1", "white-knight1", "white-bishop1", "white-queen", "white-pawn1"];
    var piecesMix = ["rook", "knight", "bishop", "queen", "pawn"];
    document.getElementById('check-status-div').style.display = "none";
    index_i = 0;
    index_j = 0;
    index_w_i = 0;
    index_w_j = 0;

    for (i = 0; i < mainChessBoard.length; i++) {
        for (j = 0; j < mainChessBoard.length; j++) {
            if (mainChessBoard[i][j] == "black-king") {
                index_i = i;
                index_j = j;
            }
            if (mainChessBoard[i][j] == "white-king") {
                index_w_i = i;
                index_w_j = j;
            }
        }
    }
    outer: for (let k = 0; k < blackArr.length; k++) {
        var movesArr = findMovement(blackArr[k], movementArray[index_i][index_j]);
        for (i = 0; i < movesArr.length; i++) {
            let dropableDivId = movementArray[movesArr[i][0]][movesArr[i][1]];
            if (document.getElementById(dropableDivId).hasChildNodes()) {
                var childId = document.getElementById(dropableDivId).childNodes[0].id;
                if (childId.includes(piecesMix[k]) && childId.includes("white")) {
                    document.getElementById('check-status-div').style.display = "block";
                    document.getElementById('checkStatus').innerHTML = "Black King Check";;
                    blackKingInCheck = true;
                    break outer;
                } else {
                    blackKingInCheck = false;
                }
            } else {
                blackKingInCheck = false;
            }
        }
    }

    outer: for (let k = 0; k < whiteArr.length; k++) {
        var movesArr = findMovement(whiteArr[k], movementArray[index_w_i][index_w_j]);
        for (i = 0; i < movesArr.length; i++) {
            let dropableDivId = movementArray[movesArr[i][0]][movesArr[i][1]];
            if (document.getElementById(dropableDivId).hasChildNodes()) {
                var childId = document.getElementById(dropableDivId).childNodes[0].id;
                if (childId.includes(piecesMix[k]) && childId.includes("black")) {
                    document.getElementById('check-status-div').style.display = "block";
                    document.getElementById('checkStatus').innerHTML = "White King Check";
                    whiteKingInCheck = true;
                    break outer;
                } else {
                    whiteKingInCheck = false;
                }
            } else {
                whiteKingInCheck = false;
            }
        }
    }
}

function findMovement(pieceId, pieceBoxId) {
    var idArray = movementArray;
    var piecesArray = mainChessBoard;
    boxIndex_i = 0;
    boxIndex_j = 0;
    var movesArr = [];
    var index = 0;

    // finding position of Div or Box
    for (i = 0; i < idArray.length; i++) {
        for (j = 0; j < idArray.length; j++) {
            if (idArray[i][j] == pieceBoxId) {
                boxIndex_i = i;
                boxIndex_j = j;
            }
        }
    }
    //Black-White Bishop
    if (pieceId == "black-bishop1" || pieceId == "black-bishop2" ||
        pieceId == "white-bishop1" || pieceId == "white-bishop2") {
        movesArr = BishopsTotalMoves(boxIndex_i, boxIndex_j, pieceId);
    }
    // Black rook and white Rook
    if (pieceId == "black-rook1" || pieceId == "black-rook2" ||
        pieceId == "white-rook1" || pieceId == "white-rook2") {
        movesArr = rookTotalMoves(boxIndex_i, boxIndex_j, pieceId);
    }
    //Black-White Queen
    if (pieceId == "black-queen" || pieceId == "white-queen") {
        firstMove = BishopsTotalMoves(boxIndex_i, boxIndex_j, pieceId);
        secondMove = rookTotalMoves(boxIndex_i, boxIndex_j, pieceId);
        movesArr = firstMove.concat(secondMove);
    }

    // For Black-Pawn
    if (pieceId == "black-pawn1" || pieceId == "black-pawn2" || pieceId == "black-pawn3" || pieceId == "black-pawn4" ||
        pieceId == "black-pawn5" || pieceId == "black-pawn6" || pieceId == "black-pawn7" || pieceId == "black-pawn8") {
        i = boxIndex_i + 1;
        if (i < 8 && boxIndex_j + 1 < 8 && piecesArray[i][boxIndex_j + 1].includes("white")) {
            movesArr[index++] = [i, boxIndex_j + 1];
        }
        if (i < 8 && boxIndex_j - 1 >= 0 && piecesArray[i][boxIndex_j - 1].includes("white")) {
            movesArr[index++] = [i, boxIndex_j - 1];
        }
        if (piecesArray[i][boxIndex_j] == -1) {
            movesArr[index++] = [i, boxIndex_j];
        }
        if (mainChessBoard[1][0] == pieceId || mainChessBoard[1][1] == pieceId || mainChessBoard[1][2] == pieceId ||
            mainChessBoard[1][3] == pieceId || mainChessBoard[1][4] == pieceId || mainChessBoard[1][5] == pieceId ||
            mainChessBoard[1][6] == pieceId || mainChessBoard[1][7] == pieceId) {
            if (piecesArray[i + 1][boxIndex_j] == -1 && piecesArray[i][boxIndex_j] == -1) {
                movesArr[index++] = [i + 1, boxIndex_j];
            }
        }
    }
    // For White-Pawn
    if (pieceId == "white-pawn1" || pieceId == "white-pawn2" || pieceId == "white-pawn3" || pieceId == "white-pawn4" ||
        pieceId == "white-pawn5" || pieceId == "white-pawn6" || pieceId == "white-pawn7" || pieceId == "white-pawn8") {

        i = boxIndex_i - 1;
        if (i >= 0 && boxIndex_j - 1 >= 0 && piecesArray[i][boxIndex_j - 1].includes("black")) {
            movesArr[index++] = [i, boxIndex_j - 1];
        }
        if (i >= 0 && boxIndex_j + 1 < 8 && piecesArray[i][boxIndex_j + 1].includes("black")) {
            movesArr[index++] = [i, boxIndex_j + 1];
        }
        if (piecesArray[i][boxIndex_j] == -1) {
            movesArr[index++] = [i, boxIndex_j];
        }
        if (mainChessBoard[6][0] == pieceId || mainChessBoard[6][1] == pieceId || mainChessBoard[6][2] == pieceId ||
            mainChessBoard[6][3] == pieceId || mainChessBoard[6][4] == pieceId || mainChessBoard[6][5] == pieceId ||
            mainChessBoard[6][6] == pieceId || mainChessBoard[6][7] == pieceId) {
            if (piecesArray[i - 1][boxIndex_j] == -1 && piecesArray[i][boxIndex_j] == -1) {
                movesArr[index++] = [i - 1, boxIndex_j];
            }
        }
    }
    //White and Black Knight
    if (pieceId == "white-knight1" || pieceId == "white-knight2" || pieceId == "black-knight1" || pieceId == "black-knight2") {
        i = boxIndex_i;
        j = boxIndex_j;
        if (j - 1 >= 0 && i - 2 >= 0) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i - 2, j - 1, pieceId));
        }
        if (j + 1 < 8 && i - 2 >= 0) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i - 2, j + 1, pieceId));
        }
        if (i - 1 >= 0 && j + 2 < 8) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i - 1, j + 2, pieceId));
        }
        if (i + 1 < 8 && j + 2 < 8) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i + 1, j + 2, pieceId));
        }
        if (i + 2 < 8 && j - 1 >= 0) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i + 2, j - 1, pieceId));
        }
        if (i + 2 < 8 && j + 1 < 8) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i + 2, j + 1, pieceId));
        }
        if (i + 1 < 8 && j - 2 >= 0) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i + 1, j - 2, pieceId));
        }
        if (i - 1 >= 0 && j - 2 >= 0) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i - 1, j - 2, pieceId));
        }
    }
    //Black-White King
    if (pieceId == "black-king" || pieceId == "white-king") {
        let i = boxIndex_i;
        let j = boxIndex_j;

        if (j - 1 >= 0 && piecesArray[i][j - 1]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i, j - 1, pieceId));
        }
        if (i + 1 < 8 && j - 1 >= 0 && piecesArray[i + 1][j - 1]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i + 1, j - 1, pieceId));
        }
        if (i - 1 >= 0 && j - 1 >= 0 && piecesArray[i - 1][j - 1]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i - 1, j - 1, pieceId));
        }
        if (i - 1 >= 0 && piecesArray[i - 1][j]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i - 1, j, pieceId));
        }
        if (i - 1 >= 0 && j + 1 < 8 && piecesArray[i - 1][j + 1]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i - 1, j + 1, pieceId));
        }
        if (j + 1 < 8 && piecesArray[i][j + 1]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i, j + 1, pieceId));
        }
        if (i + 1 < 8 && j + 1 < 8 && piecesArray[i + 1][j + 1]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i + 1, j + 1, pieceId));
        }
        if (i + 1 < 8 && piecesArray[i + 1][j]) {
            movesArr = movesArr.concat(KingsAndKnightsMoves(i + 1, j, pieceId));
        }
    }
    return movesArr;
}

function KingsAndKnightsMoves(index_i, index_j, pieceId) {
    var piecesArray = mainChessBoard;
    let totalMovesArr = [];
    index = 0;
    if (piecesArray[index_i][index_j] == -1) {
        totalMovesArr[index++] = [index_i, index_j];
    } else {
        if (pieceId.includes("white") && piecesArray[index_i][index_j].includes("black") ||
            pieceId.includes("black") && piecesArray[index_i][index_j].includes("white")) {
            totalMovesArr[index++] = [index_i, index_j];
        }
    }
    return totalMovesArr;
}

function rookTotalMoves(boxIndex_i, boxIndex_j, pieceId) {
    var movesArr = [];
    var index = 0;
    a = boxIndex_i;
    b = boxIndex_j + 1;
    var piecesArray = mainChessBoard;
    for (b = boxIndex_j + 1; b < 8; b++) {
        if (piecesArray[a][b] != -1 && b < 8) {
            if (pieceId.includes("white") && piecesArray[a][b].includes("white") ||
                pieceId.includes("black") && piecesArray[a][b].includes("black")) {
                break;
            } else {
                movesArr[index++] = [a, b];
                break;
            }
        }
        movesArr[index++] = [a, b];
    }
    for (b = boxIndex_j - 1; b >= 0; b--) {
        if (piecesArray[a][b] != -1 && b >= 0) {
            if (pieceId.includes("white") && piecesArray[a][b].includes("white") ||
                pieceId.includes("black") && piecesArray[a][b].includes("black")) {
                break;
            } else {
                movesArr[index++] = [a, b];
                break;
            }
        }
        movesArr[index++] = [a, b];
    }
    for (a = boxIndex_i - 1, b = boxIndex_j; a >= 0; a--) {
        if (piecesArray[a][b] != -1 && a >= 0) {
            if (pieceId.includes("white") && piecesArray[a][b].includes("white") ||
                pieceId.includes("black") && piecesArray[a][b].includes("black")) {
                break;
            } else {
                movesArr[index++] = [a, b];
                break;
            }
        }
        movesArr[index++] = [a, b];
    }
    for (a = boxIndex_i + 1; a < 8; a++) {
        if (piecesArray[a][b] != -1 && b >= 0) {
            if (pieceId.includes("white") && piecesArray[a][b].includes("white") ||
                pieceId.includes("black") && piecesArray[a][b].includes("black")) {
                break;
            } else {
                movesArr[index++] = [a, b];
                break;
            }
        }
        movesArr[index++] = [a, b];
    }
    return movesArr;
}

function BishopsTotalMoves(boxIndex_i, boxIndex_j, pieceId) {
    var totalMovesArr = [];
    var piecesArray = mainChessBoard;
    var index = 0;
    //Left Down
    for (let i = boxIndex_i + 1, j = boxIndex_j - 1; j >= 0 && i < 8; i++, j--) {
        if (piecesArray[i][j] != -1) {
            if (pieceId.includes("white") && piecesArray[i][j].includes("black") ||
                pieceId.includes("black") && piecesArray[i][j].includes("white")) {
                totalMovesArr[index++] = [i, j];
                break;
            } else {
                break;
            }
        } else {
            totalMovesArr[index++] = [i, j];
        }
    }
    //Right Down
    for (let i = boxIndex_i + 1, j = boxIndex_j + 1; i < 8 && j < 8; i++, j++) {
        if (piecesArray[i][j] != -1) {
            if (pieceId.includes("white") && piecesArray[i][j].includes("black") ||
                pieceId.includes("black") && piecesArray[i][j].includes("white")) {
                totalMovesArr[index++] = [i, j];
                break;
            } else {
                break;
            }
        } else {
            totalMovesArr[index++] = [i, j];
        }
    }
    //Left Up
    for (let i = boxIndex_i - 1, j = boxIndex_j - 1; i >= 0 && j >= 0; i--, j--) {
        if (piecesArray[i][j] != -1) {
            if (pieceId.includes("white") && piecesArray[i][j].includes("black") ||
                pieceId.includes("black") && piecesArray[i][j].includes("white")) {
                totalMovesArr[index++] = [i, j];
                break;
            } else {
                break;
            }
        } else {
            totalMovesArr[index++] = [i, j];
        }
    }
    //Right Up
    for (let i = boxIndex_i - 1, j = boxIndex_j + 1; i >= 0 && j < 8; i--, j++) {
        if (piecesArray[i][j] != -1) {
            if (pieceId.includes("white") && piecesArray[i][j].includes("black") ||
                pieceId.includes("black") && piecesArray[i][j].includes("white")) {
                totalMovesArr[index++] = [i, j];
                break;
            } else {
                break;
            }
        } else {
            totalMovesArr[index++] = [i, j];
        }
    }
    return totalMovesArr;
}